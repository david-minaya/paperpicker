package com.davidminaya.paperpicker

import org.junit.Test

import org.junit.Assert.*
import java.io.*
import java.math.BigInteger
import java.nio.file.Paths
import java.security.MessageDigest
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun addItemWithIndexInList() {

        val lista = MutableList(100) {""}

        lista.add(10, "Item 1")
        lista.add(15, "Item 2")
        lista.add(23, "Item 3")

        for (item in lista) {
            val index = lista.indexOf(item)
            println("$index $item")
        }
    }

    @Test
    fun createDirectoriyWithKotlin() {

        val directory = File("C:\\Users\\USER\\Desktop\\Kotlin")
        directory.mkdir()

        val image = File("C:\\Users\\USER\\Desktop\\imagen.jpg")
        val copyImage = File("C:\\Users\\USER\\Desktop\\Kotlin\\imagen.jpg")

        // image.copyTo(copyImage)

        val bis = BufferedInputStream(FileInputStream(image))
        val bos = BufferedOutputStream(FileOutputStream(copyImage))

        var read = bis.read()
        while (read != -1) {
            bos.write(read)
            read = bis.read()
        }

        bis.close()
        bos.close()
    }

    @Test
    fun saveAndReadListInStorage() {

        val lista = mutableListOf("Uno", "Dos", "Tres", "Cuatro", "Cinco")

        FileOutputStream("lista").use {
            ObjectOutputStream(it).use { oos ->
                oos.writeObject(lista)
            }
        }

        var lista2: List<*>? = null

        FileInputStream("lista").use {
            ObjectInputStream(it).use { ois ->
                lista2 = ois.readObject() as List<*>
            }
        }

        lista2?.forEach {
            it as String
            print("$it ")
        }
    }

    @Test
    fun getHashOfFile() {

        // https://stackoverflow.com/questions/415953/how-can-i-generate-an-md5-hash

        val file = File("C:\\Users\\USER\\Desktop\\imagen.jpg")

        val md5 = MessageDigest.getInstance("MD5")
        val digest = md5.digest(file.readBytes())
        val hash = BigInteger(1, digest).toString(16)

        println(hash)
    }

    @Test
    fun `write to end of file`() {

        println("Current directory is: " + Paths.get("").toAbsolutePath().toString())

        val writer = BufferedWriter(FileWriter("log.txt", true))

        val date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss a"))

        writer.use {
            it.appendln("$date Hello World!")
        }
    }

    @Test
    fun `duplicate inputStream`() {

        val inputStream = "Hola Mundo!".byteInputStream()

        val byteOutputStream = ByteArrayOutputStream()

        inputStream.use { input ->
            byteOutputStream.use {
                input.copyTo(it)
            }
        }

        val input1 = ByteArrayInputStream(byteOutputStream.toByteArray())
        val input2 = ByteArrayInputStream(byteOutputStream.toByteArray())

        input1.use {

            var i = it.read()
            var out = ""

            while (i != -1) {
                out += i.toChar()
                i = it.read()
            }

            println(out)
        }

        input2.use {

            var i = it.read()
            var out = ""

            while (i != -1) {
                out += i.toChar()
                i = it.read()
            }

            println(out)
        }
    }

    @Test
    fun `open two inputstream from one file`() {

        val file = File("archivo.txt")

        file.createNewFile()

        val inputStream1 = file.inputStream()
        val inputStream2 = file.inputStream()

        inputStream1.use {
            var byte = it.read()
            while (byte != -1) {
                print(byte.toChar())
                byte = it.read()
            }
        }

        println()

        inputStream2.use {
            var byte = it.read()
            while (byte != -1) {
                print(byte.toChar())
                byte = it.read()
            }
        }
    }

    @Test
    fun `regex match ext files`() {

        val name = "photo.jpg"
        val isMatch = Regex(""".*\.(jpg|png|jpeg)""").matches(name)

        println(isMatch)
    }
}
