package com.davidminaya.paperpicker.repositories

import android.app.Application
import android.graphics.Bitmap
import android.os.AsyncTask
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.davidminaya.paperpicker.database.DataBase
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.entitys.DeleteWallpaper
import com.davidminaya.paperpicker.utils.Common
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.firebase.firestore.FirebaseFirestore
import java.io.File
import java.util.*

class WallpaperRepository(private val app: Application) {

    private val db: DataBase = DataBase.openDataBase(app)
    private val wallpapers = MutableLiveData<MutableList<Wallpaper>>()
    private val dp = app.resources.displayMetrics.density

    fun wallpapers(): MutableLiveData<MutableList<Wallpaper>> {
        WallpapersAsyncTask(this).execute()
        return wallpapers
    }

    fun editWallpaper(wallpaper: Wallpaper) {
        UpdateWallpaperAsyncTask(this).execute(wallpaper)
    }

    fun deleteWallpaper(wallpaper: Wallpaper) {
        DeleteWallpaperAsyncTask(this).execute(wallpaper)
    }

    /* AsyncTask -------------------------------------------------------------------- */

    private class WallpapersAsyncTask(
        private val repository: WallpaperRepository): AsyncTask<Unit, Unit, Unit>() {

        override fun doInBackground(vararg params: Unit?) {

            val wallpapers = repository.db.wallpaperDao().wallpapers()
            val removeWallpapers = mutableListOf<Wallpaper>()

            for (i in 0 until wallpapers.size) {

                val wallpaper = wallpapers[i]

                val width = (172 * repository.dp).toInt()
                val height = (216 * repository.dp).toInt()

                var bitmap: Bitmap? = null

                try {
                    bitmap = Glide.with(repository.app).asBitmap().load(wallpaper.file).submit(width, height).get()
                } catch (e: Exception) {
                    removeWallpapers.add(wallpaper)
                }

                wallpaper.run {
                    this.file = File(wallpaper.path)
                    this.bitmap = bitmap
                }
            }

            wallpapers.removeAll(removeWallpapers)

            repository.wallpapers.postValue(wallpapers)
        }
    }

    private class UpdateWallpaperAsyncTask(
        private val repository: WallpaperRepository): AsyncTask<Wallpaper, Unit, Unit>() {

        override fun doInBackground(vararg params: Wallpaper?) {
            repository.db.wallpaperDao().updateWallpapers(params[0]!!)
        }
    }

    private class DeleteWallpaperAsyncTask(
        private val repository: WallpaperRepository): AsyncTask<Wallpaper, Unit, Unit>() {

        override fun doInBackground(vararg params: Wallpaper) {

            val wallpaper = params[0]

            File(wallpaper.path).delete()
            deleteFileInAddedFolder(wallpaper)
            repository.db.wallpaperDao().deleteWallpapers(wallpaper)

            // Si hay conexion a internet borra el fondo de pantalla en Firestore y Google Drive,
            // de lo contrario lo almacena en la tabla deleteWallpaper para borrarlo cuando haya
            // conexion a internet.
            if (Common.isNetworkConnected(repository.app)) {

                deleteWallpaperInFirestoreAndGoogleDrive(wallpaper)

            } else {

                repository.db.deleteWallpaperDao().insert(DeleteWallpaper(wallpaper.md5!!, wallpaper.driveId))
            }
        }

        private fun deleteFileInAddedFolder(wallpaper: Wallpaper) {

            val uriPermissions = repository.app.contentResolver.persistedUriPermissions

            for (uriPermission in uriPermissions) {

                val folder = DocumentFile.fromTreeUri(repository.app, uriPermission.uri)
                val file = folder?.findFile(File(wallpaper.path).name)

                if (file != null) {
                    file.delete()
                    break
                }
            }
        }

        private fun deleteWallpaperInFirestoreAndGoogleDrive(wallpaper: Wallpaper) {

            val googleAccount = GoogleSignIn.getLastSignedInAccount(repository.app) ?: return
            val firestore = FirebaseFirestore.getInstance()

            val credential = GoogleAccountCredential.usingOAuth2(repository.app, Collections.singleton(DriveScopes.DRIVE_FILE))
            credential.selectedAccount = googleAccount.account

            val drive = Drive.Builder(NetHttpTransport(), GsonFactory(), credential)
                .setApplicationName("Paper Picker")
                .build()

            if (wallpaper.driveId == null) return

            val task = firestore.collection("Wallpapers").document(wallpaper.md5!!).get()
            while (!task.isComplete) { }
            val driveId = task.result?.get("driveId", String::class.java)

            firestore.collection("Wallpapers").document(wallpaper.md5!!).delete()
            drive.files().delete(driveId).execute()
        }
    }
}