package com.davidminaya.paperpicker.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.davidminaya.paperpicker.dao.DeleteWallpaperDao
import com.davidminaya.paperpicker.dao.WallpaperDao
import com.davidminaya.paperpicker.entitys.DeleteWallpaper
import com.davidminaya.paperpicker.entitys.Wallpaper

@Database( entities = [Wallpaper::class, DeleteWallpaper::class], version = 9)
abstract class DataBase: RoomDatabase() {

    abstract fun wallpaperDao(): WallpaperDao
    abstract fun deleteWallpaperDao(): DeleteWallpaperDao

    companion object {

        private lateinit var db: DataBase

        fun openDataBase(context: Context): DataBase {

            synchronized(DataBase::class) {
                db = Room.databaseBuilder(context.applicationContext, DataBase::class.java, "database.db")
                    .addMigrations(MIGRATION_1_2)
                    .addMigrations(MIGRATION_2_3)
                    .addMigrations(MIGRATION_3_4)
                    .addMigrations(MIGRATION_4_5)
                    .addMigrations(MIGRATION_5_6)
                    .addMigrations(MIGRATION_6_7)
                    .addMigrations(MIGRATION_7_8)
                    .addMigrations(MIGRATION_8_9)
                    .build()
            }

            return db
        }

        private val MIGRATION_1_2 = object: Migration(1, 2) {

            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("""
                    CREATE TABLE new_wallpaper (
                        wallpaperId INTEGER PRIMARY KEY,
                        name TEXT NOT NULL,
                        path TEXT NOT NULL,
                        date TEXT NOT NULL,
                        url TEXT NOT NULL
                    )
                """)

                database.execSQL("""
                    INSERT INTO new_wallpaper
                    SELECT wallpaperId,
                        nombre as name,
                        ruta as path,
                        fecha as date,
                        url
                    FROM wallpaper
                """)

                database.execSQL("DROP TABLE wallpaper")

                database.execSQL("ALTER TABLE new_wallpaper RENAME TO wallpaper")

                database.execSQL("ALTER TABLE wallpaper ADD source INTEGER NOT NULL DEFAULT 0")
            }
        }

        private val MIGRATION_2_3 = object : Migration(2, 3) {

            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("ALTER TABLE wallpaper ADD driveId TEXT")
            }
        }

        private val MIGRATION_3_4 = object : Migration(3, 4) {

            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE wallpaper ADD md5 TEXT")
            }
        }

        private val MIGRATION_4_5 = object : Migration(4, 5) {

            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("""
                    CREATE TABLE DeleteWallpaper (
                        deleteWallpaperId INTEGER PRIMARY KEY,
                        md5 TEXT
                    )
                """)
            }
        }

        private val MIGRATION_5_6 = object : Migration(5, 6) {

            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE DeleteWallpaper ADD deleteDriveId TEXT")
            }
        }

        private val MIGRATION_6_7 = object : Migration(6, 7) {

            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("""
                    CREATE TABLE Folder (
                        folderId INTEGER PRIMARY KEY,
                        name TEXT,
                        path TEXT
                    )
                """)
            }
        }

        private val MIGRATION_7_8 = object : Migration(7, 8) {

            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("DROP TABLE Folder")
            }
        }

        private val MIGRATION_8_9 = object : Migration(8, 9) {

            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("""
                    CREATE TABLE new_wallpaper (
                        wallpaperId INTEGER PRIMARY KEY,
                        name TEXT NOT NULL,
                        path TEXT NOT NULL,
                        date TEXT NOT NULL,
                        url TEXT NOT NULL,
                        driveId TEXT,
                        md5 TEXT
                    )
                """)

                database.execSQL("""
                   INSERT INTO new_wallpaper
                   SELECT wallpaperId,
                          name,
                          path,
                          date,
                          url,
                          driveId,
                          md5
                   FROM wallpaper
                """)

                database.execSQL("""
                    DROP TABLE wallpaper
                """)

                database.execSQL("ALTER TABLE new_wallpaper RENAME TO wallpaper")
            }
        }
    }
}