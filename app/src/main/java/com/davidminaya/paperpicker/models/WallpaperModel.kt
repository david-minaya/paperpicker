package com.davidminaya.paperpicker.models

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.repositories.WallpaperRepository

class WallpaperModel(app: Application): AndroidViewModel(app) {

    private val repository = WallpaperRepository(app)
    var isFullScreen = MutableLiveData<Boolean>()
    val nombre = MutableLiveData<String>()

    init {
        isFullScreen.postValue(true)
    }

    fun editWallpaper(wallpaper: Wallpaper) {
        repository.editWallpaper(wallpaper)
    }

    fun setNombre(nombre: String) {
        this.nombre.postValue(nombre)
    }

    fun deleteWallpaper(wallpaper: Wallpaper) {
        repository.deleteWallpaper(wallpaper)
    }
}