package com.davidminaya.paperpicker.models

import android.app.Application
import androidx.lifecycle.*
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.repositories.WallpaperRepository

class MainModel(app: Application): AndroidViewModel(app) {

    private val repository = WallpaperRepository(app)

    fun wallpapers(): MutableLiveData<MutableList<Wallpaper>> {
        return repository.wallpapers()
    }
}