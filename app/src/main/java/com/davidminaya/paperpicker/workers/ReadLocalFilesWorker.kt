package com.davidminaya.paperpicker.workers

import android.content.Context
import android.content.Intent
import androidx.documentfile.provider.DocumentFile
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.work.*
import com.davidminaya.paperpicker.database.DataBase
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.utils.Common
import com.davidminaya.paperpicker.utils.ACTION_SYNC_WALLPAPER_FINISH
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.io.File

class ReadLocalFilesWorker(
    private val context: Context,
    workerParameters: WorkerParameters
) : Worker(context, workerParameters) {

    private val wallpaperDao = DataBase.openDataBase(context).wallpaperDao()
    private val hashes = wallpaperDao.md5Hashes()
    private val paperPickerDirectory = Common.getPaperPickerDirectory()
    private val newWallpapers = mutableListOf<Wallpaper>()

    override fun doWork(): Result {

        readFolder(DocumentFile.fromFile(paperPickerDirectory))
        val uriPermissions = context.contentResolver.persistedUriPermissions

        for (uriPermission in uriPermissions) {
            readFolder(DocumentFile.fromTreeUri(context, uriPermission.uri))
        }

        wallpaperDao.insert(*newWallpapers.toTypedArray())

        if (!Common.isNetworkConnected(context))
            LocalBroadcastManager.getInstance(context).sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))

        runWorkers()

        return Result.success()
    }

    private fun readFolder(folder: DocumentFile?) {

        if (folder?.listFiles() == null) return

        val listFiles = folder.listFiles().filter { Regex(""".*\.(jpg|png|jpeg)""").matches(it.name!!) }

        for (file in listFiles) {

            val bytes = context.contentResolver.openInputStream(file.uri)!!.readBytes()
            val hash = Common.calcMd5Hash(bytes)

            if (!hashes.any { it == hash }) {

                val copyFile = File(paperPickerDirectory.absolutePath + "/" + file.name)

                if (!copyFile.exists()) {

                    val inputStream = context.contentResolver.openInputStream(file.uri)!!

                    inputStream.use { input ->
                        copyFile.outputStream().use { output ->
                            input.copyTo(output)
                        }
                    }
                }

                val wallpaper = Wallpaper().apply {
                    name = file.name!!
                    this.path = copyFile.absolutePath
                    date = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)
                    md5 = hash
                }

                newWallpapers.add(wallpaper)
            }
        }
    }

    private fun runWorkers() {

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        val uploadWorker = OneTimeWorkRequestBuilder<UploadWorker>()
            .setConstraints(constraints)
            .build()

        val deleteWorker = OneTimeWorkRequestBuilder<DeleteWallpaperWorker>()
            .setConstraints(constraints)
            .build()

        val downloadWorker = OneTimeWorkRequestBuilder<DownloadWorker>()
            .setConstraints(constraints)
            .build()

        WorkManager.getInstance()
            .beginUniqueWork("Sync Works", ExistingWorkPolicy.KEEP, uploadWorker)
            .then(deleteWorker)
            .then(downloadWorker)
            .enqueue()
    }
}