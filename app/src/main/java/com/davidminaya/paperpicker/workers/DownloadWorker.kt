package com.davidminaya.paperpicker.workers

import android.content.Context
import android.content.Intent
import androidx.work.WorkerParameters
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.utils.*
import java.io.FileOutputStream
import java.io.IOException

class DownloadWorker(context: Context, workerParameters: WorkerParameters): LoadWorker(context, workerParameters) {

    override fun doWork(): Result {

        if (googleAccount == null) {
            localBroadcastManager.sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))
            return Result.failure()
        }

        val task = firestore.collection("Wallpapers").get()
        task.await()

        if (task.isCanceled) {
            localBroadcastManager.sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))
            return Result.retry()
        }

        val documents = task.result?.documents
        val wallpapers = db.wallpaperDao().wallpapers()
        val paperPickerDir = Common.getPaperPickerDirectory()

        for (document in documents!!) {

            val wallpaperAlreadyExists = wallpapers.find { it.md5 == document.id } != null

            if (wallpaperAlreadyExists) continue

            val driveFile = drive.files().get(document["driveId", String::class.java])
            val fileName = driveFile.execute().name
            val file = java.io.File(paperPickerDir, fileName)

            try {

                val outputStream = FileOutputStream(file)
                driveFile.executeMediaAndDownloadTo(outputStream)

            } catch (e : IOException) {

                file.delete()
                continue
            }

            val wallpaper = Wallpaper().apply {
                name = document["name", String::class.java] ?: ""
                path = file.absolutePath
                date = document["date", String::class.java] ?: ""
                url = document["url", String::class.java] ?: ""
                driveId = document["driveId", String::class.java]
                md5 = document.id
            }

            db.wallpaperDao().insert(wallpaper)
            localBroadcastManager.sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))
        }

        val intent = Intent(ACTION_SYNC_WALLPAPER_FINISH)
        intent.putExtra(EXTRA_IS_FINISH, true)

        localBroadcastManager.sendBroadcast(intent)
        return Result.success()
    }
}