package com.davidminaya.paperpicker.workers

import android.content.Context
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.davidminaya.paperpicker.database.DataBase
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.drive.model.File
import com.google.firebase.firestore.FirebaseFirestore
import java.util.*

abstract class LoadWorker(private val context: Context, workerParameters: WorkerParameters): Worker(context, workerParameters) {

    protected var googleAccount = GoogleSignIn.getLastSignedInAccount(context)
    protected var localBroadcastManager = LocalBroadcastManager.getInstance(context)
    protected lateinit var firestore: FirebaseFirestore
    protected lateinit var drive: Drive
    protected lateinit var db: DataBase
    protected lateinit var paperPickerDirId: String

    init {

        if (googleAccount != null) {
            firestore = FirebaseFirestore.getInstance()
            drive = getGoogleDriveInstance()
            db = DataBase.openDataBase(context)
            paperPickerDirId = getPaperPickerDirectory()
        }
    }

    private fun getGoogleDriveInstance(): Drive {

        val credential = GoogleAccountCredential.usingOAuth2(context, Collections.singleton(DriveScopes.DRIVE_FILE))
        credential.selectedAccount = googleAccount?.account

        return Drive.Builder(NetHttpTransport(), GsonFactory(), credential)
            .setApplicationName("Paper Picker")
            .build()
    }

    /**
     * Obtiene el directorio Paper picker desde Google Drive. Si el directorio
     * no existe, lo crea y después lo optiene.
     * */
    private fun getPaperPickerDirectory(): String {

        val results = drive.files().list().setQ("name='Paper picker'").execute().files

        // Log.i("LoadWorker", "obteniendo carpeta Paper Picker...")

        if (results.isNotEmpty()) {
            // Log.i("LoadWorker", "la carpeta Paper Picker ya existe")
            return results[0].id
        }

        val fileMetadata = File().apply {
            name = "Paper picker"
            mimeType = "application/vnd.google-apps.folder"
        }

        val result = drive.files().create(fileMetadata).setFields("id").execute()

        // Log.i("LoadWorker", "carpeta Paper Picker creada")
        return result.id
    }
}