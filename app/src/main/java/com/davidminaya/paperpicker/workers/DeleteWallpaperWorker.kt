package com.davidminaya.paperpicker.workers

import android.content.Context
import android.util.Log
import androidx.work.WorkerParameters

class DeleteWallpaperWorker(
    context: Context,
    workerParameters: WorkerParameters
): LoadWorker(context, workerParameters) {

    override fun doWork(): Result {

        try {

            Log.i("Worker", "---------------------------------------------")
            Log.i("DeleteWallpaperWorker", "running Delete Wallpaper Worker...")

            val deleteWallpapers = db.deleteWallpaperDao().deletesWallpapers()

            for (deleteWallpaper in deleteWallpapers) {

                firestore.collection("Wallpapers").document(deleteWallpaper.md5!!).delete()
                drive.files().delete(deleteWallpaper.deleteDriveId).execute()
                db.deleteWallpaperDao().delete(deleteWallpaper)

                Log.i("DeleteWallpaperWorker", "wallpaper ${deleteWallpaper.md5} deleted")
            }

            return Result.success()

        } catch (e: Exception) {

            return Result.failure()
        }
    }

}