package com.davidminaya.paperpicker.workers

import android.annotation.SuppressLint
import android.app.WallpaperManager
import android.content.Context
import android.graphics.Bitmap
import android.os.Environment
import android.preference.PreferenceManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.bumptech.glide.Glide
import com.davidminaya.paperpicker.database.DataBase
import com.davidminaya.paperpicker.utils.Common
import com.davidminaya.paperpicker.utils.KEY_POSITION_LIST_WALLPAPER
import org.threeten.bp.LocalTime
import java.io.File
import java.io.FileWriter

class ChangeWallpaperWorker(
    private val context: Context,
    workerParameters: WorkerParameters
) : Worker(context, workerParameters) {

    @SuppressLint("ApplySharedPref")
    override fun doWork(): Result {

        try {

            var out = ""

            val preference = PreferenceManager.getDefaultSharedPreferences(context)
            val db = DataBase.openDataBase(context)

            var position = preference.getInt(KEY_POSITION_LIST_WALLPAPER, 0)
            val wallpapersPaths = db.wallpaperDao().wallpapersPaths()

            if (position > wallpapersPaths.lastIndex) position = 0

            val wallpaperPath = wallpapersPaths[position]
            val bitmap = Glide.with(context).asBitmap().load(File(wallpaperPath)).submit().get()

            out += "--------------------------------------------------\n"
            out += "time: " +LocalTime.now().toString()+ "\n"
            out += "wallpapersPaths.size: " + wallpapersPaths.size + "\n"
            out += "position: $position \n"
            out += "wallpaperPath $wallpaperPath \n"

            val display = context.resources.displayMetrics
            val gcd = Common.calcGreatestCommonDivisor(display.widthPixels, display.heightPixels)
            val widthAspectRatio = display.widthPixels / gcd
            val heightAspectRatio = display.heightPixels / gcd

            var uva: Int
            var width: Int
            var height: Int

            uva = if (widthAspectRatio > heightAspectRatio) {
                bitmap.width / widthAspectRatio
            } else {
                bitmap.height / heightAspectRatio
            }

            width = uva * widthAspectRatio
            height = uva * heightAspectRatio

            if (width > bitmap.width) {
                uva = bitmap.width / widthAspectRatio
            }

            if (height > bitmap.height) {
                uva = bitmap.height / heightAspectRatio
            }

            width = uva * widthAspectRatio
            height = uva * heightAspectRatio

            val offsetW = (bitmap.width - width) / 2
            val offsetH = (bitmap.height - height) / 2
            val newBitmap = Bitmap.createBitmap(bitmap, offsetW, offsetH, bitmap.width - offsetW, bitmap.height - offsetH)

            val wallpaperManager = context.getSystemService(Context.WALLPAPER_SERVICE) as WallpaperManager
            wallpaperManager.setBitmap(newBitmap)

            position = if (position < wallpapersPaths.lastIndex) ++position else 0

            out += "position < wallpapersPaths.lastIndex: " +(position < wallpapersPaths.lastIndex)+ "\n"
            out += "position: $position"

            preference.edit().run {
                putInt(KEY_POSITION_LIST_WALLPAPER, position)
                commit()
            }

            FileWriter(Environment.getExternalStorageDirectory().absolutePath + "/ChangeWallpaper.txt", true).use {
                it.appendln(out)
            }

            return Result.success()

        } catch (e: Exception) {

            return Result.retry()
        }
    }
}