package com.davidminaya.paperpicker.workers

import android.content.Context
import android.content.Intent
import androidx.work.WorkerParameters
import com.davidminaya.paperpicker.utils.ACTION_SYNC_WALLPAPER_FINISH
import com.davidminaya.paperpicker.utils.await
import com.google.api.client.http.FileContent
import com.google.api.services.drive.model.File

class UploadWorker(context: Context, parameter: WorkerParameters): LoadWorker(context, parameter) {

    override fun doWork(): Result {

        if (googleAccount == null) {
            localBroadcastManager.sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))
            return Result.failure()
        }

        val wallpapers = db.wallpaperDao().wallpapers()

        val getting = firestore.collection("Wallpapers").get()
        getting.await()
        val documents = getting.result?.documents

        if (getting.isCanceled) {
            localBroadcastManager.sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))
            return Result.retry()
        }

        for (wallpaper in wallpapers) {

            val document = documents?.find { it.id == wallpaper.md5 }
            wallpaper.file = java.io.File(wallpaper.path)

            if (document != null) {

                if (wallpaper.driveId == null) {
                    wallpaper.driveId = document["driveId", String::class.java]
                    db.wallpaperDao().updateWallpapers(wallpaper)
                }

                val updateWallpaper = mutableMapOf<String, Any>("date" to wallpaper.date, "driveId" to wallpaper.driveId!!)

                if (document["name"] == wallpaper.file?.name && wallpaper.file?.name != wallpaper.name) updateWallpaper["name"] = wallpaper.name
                if (document["url"].toString().isEmpty() && wallpaper.url.isNotBlank()) updateWallpaper["url"] =  wallpaper.url

                if (document["name"] != wallpaper.name || document["url"].toString().isNotBlank()) {
                    val updating = firestore.collection("Wallpapers").document(document.id).update(updateWallpaper)
                    updating.await()
                }

                continue
            }

            wallpaper.file = java.io.File(wallpaper.path)

            val fileDrive = File().apply {
                name = wallpaper.file?.name
                parents = mutableListOf(paperPickerDirId)
            }

            // Sube la imagen a Google Drive
            val fileContent = FileContent("image/*", wallpaper.file)
            val file = drive.files().create(fileDrive, fileContent).execute()

            if (file != null) {

                wallpaper.driveId = file.id

                val wallpaperMap = mapOf(
                    "name" to wallpaper.name,
                    "date" to wallpaper.date,
                    "url" to wallpaper.url,
                    "driveId" to wallpaper.driveId
                )

                val adding = firestore.collection("Wallpapers")
                    .document(wallpaper.md5!!)
                    .set(wallpaperMap)

                adding.await()

                if (adding.isCanceled) {
                    localBroadcastManager.sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))
                    return Result.retry()
                }

                db.wallpaperDao().updateWallpapers(wallpaper)

            } else {

                localBroadcastManager.sendBroadcast(Intent(ACTION_SYNC_WALLPAPER_FINISH))
                return Result.retry()
            }
        }

        return Result.success()
    }
}