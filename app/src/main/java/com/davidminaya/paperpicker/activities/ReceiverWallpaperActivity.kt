package com.davidminaya.paperpicker.activities

import android.annotation.SuppressLint
import android.app.WallpaperManager
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.HandlerThread
import android.provider.OpenableColumns
import android.view.*
import android.webkit.URLUtil
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.database.DataBase
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.utils.Common
import kotlinx.android.synthetic.main.activity_receiver_wallpaper.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.io.File
import java.io.FileOutputStream
import java.util.*

@SuppressLint("InflateParams")
class ReceiverWallpaperActivity: AppCompatActivity() {

    private lateinit var uri: Uri
    private lateinit var ext: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receiver_wallpaper)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close)
        supportActionBar?.title = ""

        uri = intent.getParcelableExtra(Intent.EXTRA_STREAM)

        val cursor = contentResolver.query(uri, null, null, null, null)
        val nameIndex = cursor?.getColumnIndex(OpenableColumns.DISPLAY_NAME)

        cursor?.moveToFirst()

        Glide.with(this).load(uri).into(imagen)
        nombre.text = cursor?.getString(nameIndex!!)
        ext = nombre.text.split(".").last()

        val date = LocalDate.now().format(DateTimeFormatter.ofPattern("EEEE dd 'de' MMMM 'del' YYYY", Locale.forLanguageTag("es")))
        fecha.text = date

        cursor?.close()
    }

    fun onClickEditarNombreListener(view: View) {

        val inflater = LayoutInflater.from(this).inflate(R.layout.dialog_edit_name, null)
        val editarNombre = inflater.findViewById<EditText>(R.id.editarNombre)
        editarNombre.setText(nombre.text)

        val dialog = AlertDialog.Builder(this).apply {
            setTitle(getString(R.string.editar_nombre))
            setView(inflater)
        }

        dialog.setPositiveButton(getString(R.string.aceptar)) { _, _ ->

            val nombre = editarNombre.text.toString()

            if (nombre.isBlank() && nombre == this.nombre.text)
                return@setPositiveButton

            this.nombre.text = nombre
        }

        dialog.setNegativeButton(getString(R.string.cancelar)) { _, _ ->  }

        dialog.create().show()
    }

    fun onClickAgregarUrlListener(view: View) {

        val inflater = LayoutInflater.from(this).inflate(R.layout.dialog_add_url, null)
        val paste = inflater.findViewById<ImageView>(R.id.paste)
        val addUrl = inflater.findViewById<EditText>(R.id.add_url)

        val dialog = AlertDialog.Builder(this)
            .setTitle(getString(R.string.editar_nombre))
            .setView(inflater)
            .setPositiveButton(getString(R.string.aceptar), null)
            .setNegativeButton(getString(R.string.cancelar), null)
            .create()

        paste.setOnClickListener {

            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val text = clipboard.primaryClip?.getItemAt(0)?.text ?: ""

            addUrl.setText(text)
        }

        dialog.setOnShowListener {

            val button = (it as AlertDialog)

            button.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {

                when {

                    addUrl.text.isNullOrBlank() -> {

                        dialog.dismiss()
                    }

                    !URLUtil.isValidUrl(addUrl.text.toString()) -> {

                        addUrl.error = "Url invalida"
                    }

                    else -> {

                        imagen3.setImageResource(R.drawable.ic_internet)

                        agregarUrl.run {
                            isClickable = false
                            isFocusable = false
                            setBackgroundColor(Color.parseColor("#00000000"))
                        }

                        url.run {
                            text = addUrl.text.toString()
                            setTextColor(Color.BLACK)
                        }

                        dialog.dismiss()
                    }
                }
            }
        }

        dialog.show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_receiver_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {

            android.R.id.home -> {

                finish()
                return true
            }

            R.id.aceptar -> {

                savedWallpaper()
                return true
            }
        }

        return false
    }

    private fun savedWallpaper() {

        val thread = HandlerThread("Receiver wallpaper activity thread")
        thread.start()

        Handler(thread.looper).post {

            val wallpaperDao = DataBase.openDataBase(this).wallpaperDao()
            var path = "${Common.getPaperPickerDirectory().absolutePath}/${nombre.text}"
            path += if (!nombre.text.contains(".$ext")) ".$ext" else ""
            val inputStream1 = contentResolver.openInputStream(uri)
            val inputStream2 = contentResolver.openInputStream(uri)

            // Confirma si el fondo de pantalla existe

            val md5 = Common.calcMd5Hash(inputStream1!!.readBytes())

            if (wallpaperDao.md5Hashes().any { it == md5 }) {

                runOnUiThread {
                    Toast.makeText(this, getString(R.string.wallpaper_already_exists), Toast.LENGTH_SHORT).show()
                }

            } else {

                // Copia la imagen en el directorio Paper picker

                inputStream2.use { input ->
                    FileOutputStream(path).use { output ->
                        input!!.copyTo(output)
                    }
                }

                // Guarda el fondo de pantalla en la base de datos

                val wallpaper = Wallpaper()
                wallpaper.name = nombre.text.toString()
                wallpaper.date = LocalDate.now().format(DateTimeFormatter.ISO_LOCAL_DATE)
                wallpaper.path = path
                wallpaper.md5 = md5
                wallpaper.url = if (url.text.toString() != getString(R.string.agregar_url)) url.text.toString() else ""

                wallpaperDao.insert(wallpaper)

                // Establece el fondo de pantallas

                if (checkbox.isChecked) {

                    runOnUiThread {
                        Toast.makeText(this, getString(R.string.estableciendo_fondo), Toast.LENGTH_SHORT).show()
                    }

                    val wallpaperManager = getSystemService(Context.WALLPAPER_SERVICE) as WallpaperManager
                    val display = resources.displayMetrics
                    val bitmap = Glide.with(this).asBitmap().load(File(path)).submit(display.widthPixels, display.heightPixels).get()

                    val gcd = Common.calcGreatestCommonDivisor(display.widthPixels, display.heightPixels)
                    val widthAspectRatio = display.widthPixels / gcd
                    val heightAspectRatio = display.heightPixels / gcd

                    var uva: Int
                    var width: Int
                    var height: Int

                    uva = if (widthAspectRatio > heightAspectRatio) {
                        bitmap.width / widthAspectRatio
                    } else {
                        bitmap.height / heightAspectRatio
                    }

                    width = uva * widthAspectRatio
                    height = uva * heightAspectRatio

                    if (width > bitmap.width) {
                        uva = bitmap.width / widthAspectRatio
                    }

                    if (height > bitmap.height) {
                        uva = bitmap.height / heightAspectRatio
                    }

                    width = uva * widthAspectRatio
                    height = uva * heightAspectRatio

                    val offsetW = (bitmap.width - width) / 2
                    val offsetH = (bitmap.height - height) / 2
                    val newBitmap = Bitmap.createBitmap(bitmap, offsetW, offsetH, bitmap.width - offsetW, bitmap.height - offsetH)

                    wallpaperManager.setBitmap(newBitmap)

                    runOnUiThread {
                        Toast.makeText(this, getString(R.string.fondo_establecido), Toast.LENGTH_SHORT).show()
                    }
                }
            }

            finish()
        }
    }
}