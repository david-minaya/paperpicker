package com.davidminaya.paperpicker.activities

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.fragments.LoginFragment
import com.davidminaya.paperpicker.fragments.WelcomeFragment
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity() {

    private lateinit var preferencias: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        preferencias = PreferenceManager.getDefaultSharedPreferences(this)

        val fragments = mutableListOf(

            WelcomeFragment.newInstance(R.raw.image1,
                "Establece fondos de pantalla desde cualquier lugar",
                "Las redes sociales, la web, tu dispositivo u otros equipos. Es tan fácil como compartir una imagen o una url."),

            WelcomeFragment.newInstance(R.raw.image2,
                "Crea tu propia galeria de fondos de pantalla",
                "Donde todos los fondos de pantalla son tus favoritos"),

            WelcomeFragment.newInstance(R.raw.image3,
                "Almacena tus fondos de pantalla en Google Drive",
                "Para que puedas acceder a ellos desde cualquier lugar"),

            LoginFragment()
        )

        viewPager.adapter = ViewPageAdapter(supportFragmentManager, fragments)
        tabLayout.setupWithViewPager(viewPager, true)
    }

    private inner class ViewPageAdapter(supportFragmentManager: FragmentManager,
        private val fragments: List<Fragment>): FragmentPagerAdapter(supportFragmentManager) {

        override fun getItem(position: Int): Fragment = fragments[position]

        override fun getCount(): Int = fragments.size
    }
}
