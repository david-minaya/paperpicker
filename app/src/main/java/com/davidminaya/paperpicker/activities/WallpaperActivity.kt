package com.davidminaya.paperpicker.activities

import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.fragments.WallpaperFragment
import com.davidminaya.paperpicker.models.WallpaperModel
import com.davidminaya.paperpicker.utils.*
import kotlinx.android.synthetic.main.activity_wallpaper.*
import androidx.constraintlayout.widget.ConstraintLayout
import android.os.Handler
import android.os.HandlerThread
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.viewpager.widget.PagerAdapter
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.fragments.InfoFragment
import java.io.File

class WallpaperActivity: AppCompatActivity() {

    private lateinit var model: WallpaperModel
    private val fragments = mutableListOf<WallpaperFragment>()
    private lateinit var wallpapers: List<Wallpaper>
    private lateinit var adapter: FragmentPageAdapter
    private lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallpaper)

        window?.decorView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

        model = ViewModelProviders.of(this)[WallpaperModel::class.java]
        model.isFullScreen.observe(this, onFullScreenListener)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        val position = intent.getIntExtra(EXTRA_WALLPAPER_POSITION, 0)
        wallpapers = intent.getParcelableArrayListExtra(EXTRA_WALLPAPERS)

        setSupportActionBar(toolbar)
        supportActionBar?.title = wallpapers[position].name
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        marginSystemBar(toolbar, "status_bar_height") { p, m -> p.topMargin = m }
        if (hasNavBar()) marginSystemBar(optionsBar, "navigation_bar_height") { p, m -> p.bottomMargin = m }

        wallpapers.forEach {
            it.file = File(it.path)
            val fragment = WallpaperFragment.newInstance(it)
            fragments.add(fragment)
        }

        adapter = FragmentPageAdapter(supportFragmentManager, fragments)

        viewPager.adapter = adapter
        viewPager.currentItem = position
        viewPager.addOnPageChangeListener(onPageChangeListener)

        optionsBar.setOnOptionsClickListener(this::onOptionsClickListener)

        model.nombre.observe(this, Observer {
            wallpapers[position].name = it
            supportActionBar?.title = it
        })
    }

    private val onPageChangeListener = object: ViewPager.OnPageChangeListener {

        override fun onPageScrollStateChanged(state: Int) {}

        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int){}

        override fun onPageSelected(position: Int) {
            supportActionBar?.title = wallpapers[position].name
        }
    }

    private val onFullScreenListener = Observer<Boolean> {

        if (!it) {

            val translationToolbar = -(toolbar.height + heightSystemBar("status_bar_height")).toFloat()
            val translationBottomBar = optionsBar.height + heightSystemBar("navigation_bar_height").toFloat()

            toggleBars(translationToolbar, translationBottomBar)

            window?.decorView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE
                    or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_FULLSCREEN)

        } else {

            window?.decorView?.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)

            toggleBars()
        }
    }

    private fun onOptionsClickListener(viewId: Int) {

        when (viewId) {

            R.id.establecerFondo -> {

                val pathWallpaper = wallpapers[viewPager.currentItem].path

                Toast.makeText(this, getString(R.string.estableciendo_fondo), Toast.LENGTH_SHORT).show()

                val wallpaperManager = getSystemService(Context.WALLPAPER_SERVICE) as WallpaperManager
                val display = resources.displayMetrics
                val bitmap = BitmapFactory.decodeFile(pathWallpaper)

                val gcd = Common.calcGreatestCommonDivisor(display.widthPixels, display.heightPixels)
                val widthAspectRatio = display.widthPixels / gcd
                val heightAspectRatio = display.heightPixels / gcd

                var uva: Int
                var width: Int
                var height: Int

                uva = if (widthAspectRatio > heightAspectRatio) {
                    bitmap.width / widthAspectRatio
                } else {
                    bitmap.height / heightAspectRatio
                }

                width = uva * widthAspectRatio
                height = uva * heightAspectRatio

                if (width > bitmap.width) {
                    uva = bitmap.width / widthAspectRatio
                }

                if (height > bitmap.height) {
                    uva = bitmap.height / heightAspectRatio
                }

                width = uva * widthAspectRatio
                height = uva * heightAspectRatio

                val offsetW = (bitmap.width - width) / 2
                val offsetH = (bitmap.height - height) / 2
                val newBitmap = Bitmap.createBitmap(bitmap, offsetW, offsetH, bitmap.width - offsetW, bitmap.height - offsetH)

                val thread = HandlerThread("Wallpaper Activity thread")
                thread.start()

                Handler(thread.looper).post {

                    wallpaperManager.setBitmap(newBitmap)

                    runOnUiThread {
                        Toast.makeText(this, getString(R.string.fondo_establecido), Toast.LENGTH_SHORT).show()
                    }
                }

                preferences.edit().run {
                    putInt(KEY_POSITION_LIST_WALLPAPER, viewPager.currentItem)
                    apply()
                }
            }

            R.id.ponerEnCola -> {

                Toast.makeText(this, getString(R.string.puesto_en_cola), Toast.LENGTH_SHORT).show()

                preferences.edit().run {
                    putInt(KEY_POSITION_LIST_WALLPAPER, viewPager.currentItem)
                    apply()
                }
            }

            R.id.mostrarInformacion -> {

                val pagePosition = viewPager.currentItem
                val wallpaper = wallpapers[pagePosition]
                val infoFragment = InfoFragment.newInstance(wallpaper)
                infoFragment.show(supportFragmentManager, "Wallpaper info fragment")

                infoFragment.setOnDeleteWallpaperListener {
                    adapter.deletePage(pagePosition)
                }
            }

            R.id.Compartir -> {

                val wallpaper = wallpapers[viewPager.currentItem]

                val wallpaperUri = FileProvider.getUriForFile(this, AUTHORITY_FILE_PROVIDER, wallpaper.file!!)
                var text = "${wallpaper.name} \n"

                if (wallpaper.url.isNotBlank()) {
                    text += wallpaper.url
                }

                val intent = Intent(Intent.ACTION_SEND).apply {
                    type = "*/*"
                    putExtra(Intent.EXTRA_TEXT, text)
                    putExtra(Intent.EXTRA_STREAM, wallpaperUri)
                }

                startActivity(Intent.createChooser(intent, getString(R.string.compartir_fondo)))
            }
        }
    }

    private fun hasNavBar(): Boolean {
        val id = resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && resources.getBoolean(id)
    }

    private fun marginSystemBar(bar: View, name: String, margin: (ConstraintLayout.LayoutParams, Int) -> Unit) {

        val params = bar.layoutParams as ConstraintLayout.LayoutParams
        margin(params, heightSystemBar(name))
        bar.layoutParams = params
    }

    private fun heightSystemBar(name: String): Int {
        val resourceId = resources.getIdentifier(name, "dimen", "android")
        return if (resourceId > 0) resources.getDimensionPixelSize(resourceId) else 0
    }

    private fun toggleBars(translationToolbar: Float = 0f, translationBottomBar: Float = 0f) {

        toolbar.animate().duration = 210
        optionsBar.animate().duration = 210
        toolbar.animate().translationY(translationToolbar).start()
        optionsBar.animate().translationY(translationBottomBar).start()
    }

    private inner class FragmentPageAdapter(fm: FragmentManager,
                                            private val fragments: MutableList<WallpaperFragment>): FragmentStatePagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return fragments[position]
        }

        override fun getCount(): Int = fragments.size

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }

        fun deletePage(position: Int) {
            fragments.removeAt(position)
            notifyDataSetChanged()
        }
    }
}