package com.davidminaya.paperpicker.activities

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.fragments.SettingFragment
import kotlinx.android.synthetic.main.activity_main.*
import android.animation.AnimatorListenerAdapter
import android.view.ViewAnimationUtils
import android.animation.Animator
import android.content.SharedPreferences
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import com.davidminaya.paperpicker.fragments.MainFragment
import com.davidminaya.paperpicker.utils.Common
import kotlin.math.sqrt


class MainActivity : AppCompatActivity() {

    private lateinit var settingMenuItem: MenuItem
    private var settingFragmentIsShow = false
    private lateinit var preferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        preferences = PreferenceManager.getDefaultSharedPreferences(this)

        if (savedInstanceState == null) {
            requestExternalStoragePermissions()
        }
    }

    private fun requestExternalStoragePermissions() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE), 1)

        } else {

            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, MainFragment())
                .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        settingMenuItem = menu?.findItem(R.id.setting_fragment)!!
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item?.itemId == R.id.setting_fragment) {

            var settingFragment = supportFragmentManager.findFragmentByTag("SettingFragment")

            if (settingFragment == null) {

                supportFragmentManager.beginTransaction()
                    .add(R.id.fragment, SettingFragment(), "SettingFragment")
                    .addToBackStack(null)
                    .commit()

                item.setIcon(R.drawable.ic_settings_show)
                supportActionBar?.title = getString(R.string.configuracion)
                settingFragmentIsShow = true

                return true

            } else {

                settingFragment = supportFragmentManager.findFragmentByTag("SettingFragment")!!
                closeSettingFragment(settingFragment, item)

                return true
            }
        }

        return false
    }

    private fun closeSettingFragment(settingFragment: Fragment, item: MenuItem) {

        val view = settingFragment.view!!

        item.isEnabled = false
        val dp = resources.displayMetrics.density

        val right = view.right - (26 * dp).toInt()
        val top = view.top
        val width = view.width
        val height = view.height
        val radio = sqrt((width * width + height * height).toDouble()).toFloat()

        val anim = ViewAnimationUtils.createCircularReveal(view, right, top, radio, 0f)
        anim.duration = 550
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                item.isEnabled = true
                settingFragmentIsShow = false
                settingFragment.activity!!.onBackPressed()
            }
        })

        anim.start()

        supportActionBar?.title = getString(R.string.app_name)
        item.setIcon(R.drawable.ic_settings)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            supportFragmentManager.beginTransaction()
                .add(R.id.fragment, MainFragment())
                .commit()

            Common.runSyncWorkers()

        } else {

            finish()
        }
    }

    override fun onBackPressed() {

        if (settingFragmentIsShow) {

            val settingFragment = supportFragmentManager.findFragmentByTag("SettingFragment")!!
            closeSettingFragment(settingFragment,  settingMenuItem)

        } else {

            super.onBackPressed()
        }
    }
}