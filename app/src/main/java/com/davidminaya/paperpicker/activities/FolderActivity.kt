package com.davidminaya.paperpicker.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.TransitionManager
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.adapters.FolderAdapter
import kotlinx.android.synthetic.main.activity_folder.*

class FolderActivity: AppCompatActivity() {

    private lateinit var adapter: FolderAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_folder)

        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        adapter = FolderAdapter(this)
        adapter.onRemovedFolderListener = this::onRemovedFolderListener

        folders.layoutManager = LinearLayoutManager(this)
        folders.adapter = adapter

        getUriPermissions()
    }

    private fun getUriPermissions() {

        val uriPermissions = contentResolver.persistedUriPermissions

        if (uriPermissions.isEmpty()) {

            TransitionManager.beginDelayedTransition(rootScene)
            folders.visibility = View.GONE
            thereArentFolders.visibility = View.VISIBLE

        } else {

            TransitionManager.beginDelayedTransition(rootScene)
            thereArentFolders.visibility = View.GONE
            folders.visibility = View.VISIBLE

            adapter.uriPermissions = uriPermissions
        }
    }

    fun onClickAddFolderListener(view: View) {

        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT_TREE)
        startActivityForResult(intent, 1)
    }

    private fun onRemovedFolderListener() {
        getUriPermissions()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (resultCode == RESULT_OK) {

            contentResolver.takePersistableUriPermission(data?.data!!,
                Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)

            getUriPermissions()
        }
    }
}