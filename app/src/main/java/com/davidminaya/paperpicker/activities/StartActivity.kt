package com.davidminaya.paperpicker.activities

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import com.davidminaya.paperpicker.utils.*

class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val preferencias = PreferenceManager.getDefaultSharedPreferences(this)
        val esLaPrimeraEjecucion = preferencias.getBoolean(KEY_IS_FIRST_TIME, true)

        if (!esLaPrimeraEjecucion) {
            startActivity(Intent(this, MainActivity::class.java))
        } else {
            startActivity(Intent(this, WelcomeActivity::class.java))
        }

        finish()
    }
}
