package com.davidminaya.paperpicker.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Environment
import androidx.work.*
import com.davidminaya.paperpicker.workers.ReadLocalFilesWorker
import java.io.File
import java.math.BigInteger
import java.security.MessageDigest

class Common {

    companion object {

        fun runSyncWorkers() {

            val readFilesWorker = OneTimeWorkRequestBuilder<ReadLocalFilesWorker>()
                .build()

            WorkManager.getInstance()
                .enqueueUniqueWork("Read Files Worker", ExistingWorkPolicy.KEEP, readFilesWorker)
        }

        fun isNetworkConnected(context: Context): Boolean {

            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {

                if (connectivityManager.activeNetworkInfo != null) {
                    connectivityManager.activeNetworkInfo.isConnected
                } else {
                    false
                }

            } else {

                val networkCapabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)

                if (networkCapabilities != null) {
                    (networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI))
                } else {
                    false
                }
            }
        }

        fun getPaperPickerDirectory(): File {

            val path = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                .absolutePath + "/Wallpaper Gallery"

            val paperPickerDirectory = File(path)

            if (!paperPickerDirectory.exists()) {
                paperPickerDirectory.mkdir()
            }

            return paperPickerDirectory
        }

        fun calcMd5Hash(bytes: ByteArray): String {

            val md5 = MessageDigest.getInstance("MD5")
            val digest = md5.digest(bytes)

            return BigInteger(1, digest).toString(16)
        }

        fun calcGreatestCommonDivisor(width: Int, height: Int): Int {

            var w = width
            var h = height

            if (height > width) {
                w = height
                h = width
            }

            var resto = w % h
            var lastH = h

            while (resto != 0) {
                resto = lastH % resto
                lastH = h
                h = resto
            }

            return lastH
        }
    }
}