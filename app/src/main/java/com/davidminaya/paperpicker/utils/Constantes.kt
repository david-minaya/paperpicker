package com.davidminaya.paperpicker.utils

const val ACTION_DELETE_WALLPAPER = "com.davidminaya.paperpicker.ACTION_DELETE_WALLPAPER"
const val ACTION_EDIT_NAME_WALLPAPER = "com.davidminaya.paperpicker.ACTION_EDIT_NAME_WALLPAPER"
const val ACTION_SYNC_WALLPAPER = "com.davidminaya.paperpicker.ACTION_SYNC_WALLPAPER"
const val ACTION_SYNC_WALLPAPER_FINISH = "com.davidminaya.paperpicker.ACTION_SYNC_WALLPAPER_FINISH"

const val EXTRA_WALLPAPER = "com.davidminaya.paperpicker.EXTRA_WALLPAPER"
const val EXTRA_WALLPAPER_POSITION = "com.davidminaya.paperpicker.EXTRA_WALLPAPER_POSITION"
const val EXTRA_WALLPAPERS = "com.davidminaya.paperpicker.EXTRA_WALLPAPERS"
const val EXTRA_WALLPAPER_ID = "com.davidminaya.paperpicker.EXTRA_WALLPAPER_ID"
const val EXTRA_WALLPAPER_NAME = "com.davidminaya.paperpicker.EXTRA_WALLPAPER_NAME"
const val EXTRA_IS_FINISH = "com.davidminaya.paperpicker.EXTRA_IS_FINISH"

const val OPEN_LINK = "com.davidminaya.paperpicker.OPEN_LINK"
const val COPY_LINK = "com.davidminaya.paperpicker.COPY_LINK"
const val EDIT_NAME = "com.davidminaya.paperpicker.EDIT_NAME"
const val DELETE_WALLPAPER = "com.davidminaya.paperpicker.DELETE_WALLPAPER"

const val TAG_AUTO_CHANGE_WALLPAPER = "com.davidminaya.paperpicker.TAG_AUTOCHANGE_WALLPAPER"
const val TAG_CHANGE_WALLPAPER_EVERY = "com.davidminaya.paperpicker.TAG_CHANGE_WALLPAPER_EVERY"
const val TAG_LOGIN = "com.davidminaya.paperpicker.TAG_LOGIN"
const val TAG_LOGOUT = "com.davidminaya.paperpicker.TAG_LOGOUT"
const val TAG_DIRECTORY = "com.davidminaya.paperpicker.TAG_DIRECTORY"

const val KEY_AUTO_CHANGE_WALLPAPER = "com.davidminaya.paperpicker.KEY_AUTO_CHANGE_WALLPAPER"
const val KEY_CHANGE_WALLPAPER_EVERY = "com.davidminaya.paperpicker.KEY_CHANGE_WALLPAPER_EVERY"
const val KEY_POSITION_LIST_WALLPAPER = "com.davidminaya.paperpicker.KEY_POSITION_LIST_WALLPAPER"
const val KEY_IS_FIRST_TIME = "com.davidminaya.paperpicker.KEY_IS_FIRST_TIME"

const val AUTHORITY_FILE_PROVIDER = "com.davidminaya.paperpicker.fileprovider"