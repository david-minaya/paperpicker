package com.davidminaya.paperpicker.utils

import com.google.android.gms.tasks.Task

fun Task<*>.await() {
    while (!this.isComplete) { }
}