package com.davidminaya.paperpicker.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import com.davidminaya.paperpicker.R

class OptionsBar: ConstraintLayout {

    private lateinit var establecerFondo: ImageView
    private lateinit var ponerEnCola: ImageView
    private lateinit var mostrarInformacion: ImageView
    private lateinit var compartir: ImageView

    private lateinit var onOptionClickListener: (Int) -> Unit

    constructor(context: Context): super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?): super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int): super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {

        LayoutInflater.from(context).inflate(R.layout.options_bar, this, true)

        establecerFondo = findViewById(R.id.establecerFondo)
        ponerEnCola = findViewById(R.id.ponerEnCola)
        mostrarInformacion = findViewById(R.id.mostrarInformacion)
        compartir = findViewById(R.id.Compartir)

        establecerFondo.setOnClickListener(this::onClickListener)
        ponerEnCola.setOnClickListener(this::onClickListener)
        mostrarInformacion.setOnClickListener(this::onClickListener)
        compartir.setOnClickListener(this::onClickListener)

    }

    private fun onClickListener(view: View) {
        onOptionClickListener(view.id)
    }

    fun setOnOptionsClickListener(listener: (Int) -> Unit) {
        onOptionClickListener = listener
    }

}