package com.davidminaya.paperpicker.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.models.WallpaperModel
import com.davidminaya.paperpicker.utils.EXTRA_WALLPAPER
import kotlinx.android.synthetic.main.fragment_wallpaper.*

class WallpaperFragment: Fragment() {

    private lateinit var model: WallpaperModel
    lateinit var wallpaper: Wallpaper

    companion object {

        fun newInstance(wallpaper: Wallpaper): WallpaperFragment {

            val bundle = Bundle().apply {
                putParcelable(EXTRA_WALLPAPER, wallpaper)
            }

            val fragment = WallpaperFragment()
            fragment.arguments = bundle

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        wallpaper = arguments!!.getParcelable(EXTRA_WALLPAPER)!!
        model = ViewModelProviders.of(activity!!)[WallpaperModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(activity).inflate(R.layout.fragment_wallpaper, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this).load(wallpaper.file).into(imagen)

        imagen.setOnClickListener {
            model.isFullScreen.postValue(!model.isFullScreen.value!!)
        }
    }
}