package com.davidminaya.paperpicker.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.davidminaya.paperpicker.R
import kotlinx.android.synthetic.main.fragment_welcome.*

class WelcomeFragment: Fragment() {

    companion object {

        private const val KEY_RES = "KEY_RES"
        private const val KEY_TITLE = "KEY_TITLE"
        private const val KEY_DESCRIPTION = "KEY_DESCRIPTION"

        fun newInstance(res: Int, title: String, description: String): WelcomeFragment {

            val bundle = Bundle()
            bundle.putInt(KEY_RES, res)
            bundle.putString(KEY_TITLE, title)
            bundle.putString(KEY_DESCRIPTION, description)

            val welcomeFragment = WelcomeFragment()
            welcomeFragment.arguments = bundle

            return  welcomeFragment
        }
    }

    private var res = 0
    private lateinit var title: String
    private lateinit var description: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        res = arguments?.getInt(KEY_RES)!!
        title = arguments?.getString(KEY_TITLE)!!
        description = arguments?.getString(KEY_DESCRIPTION)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Glide.with(this).load(res).into(image)
        titleTextView.text = title
        descriptionTextView.text = description
    }
}