package com.davidminaya.paperpicker.fragments

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.activities.MainActivity
import com.davidminaya.paperpicker.utils.KEY_IS_FIRST_TIME
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.api.services.drive.DriveScopes
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment: Fragment() {

    private lateinit var preferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferences = PreferenceManager.getDefaultSharedPreferences(activity!!)

        Glide.with(activity!!).load(R.raw.login_wallpaper).into(image)

        login.setOnClickListener {

            val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.request_id_token))
                .requestEmail()
                .requestScopes(Scope(DriveScopes.DRIVE_FILE))
                .build()

            val googleSignInClient = GoogleSignIn.getClient(activity!!, gso)
            startActivityForResult(googleSignInClient.signInIntent, 21)
        }

        omitir.setOnClickListener {

            preferences.edit()
                .putBoolean(KEY_IS_FIRST_TIME, false)
                .apply()

            startActivity(Intent(activity!!, MainActivity::class.java))
            activity!!.finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 21) {

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {

                val googleAccount = task.getResult(ApiException::class.java)
                val credential = GoogleAuthProvider.getCredential(googleAccount?.idToken, null)
                FirebaseAuth.getInstance().signInWithCredential(credential)

                Toast.makeText(activity, getString(R.string.bienvenido) +" "+ googleAccount?.email, Toast.LENGTH_SHORT).show()

                preferences.edit()
                    .putBoolean(KEY_IS_FIRST_TIME, false)
                    .apply()

                startActivity(Intent(activity!!, MainActivity::class.java))
                activity!!.finish()

            } catch (e: ApiException) {

                Toast.makeText(activity, getString(R.string.mensaje_error_inicio_de_sesion), Toast.LENGTH_SHORT).show()
            }
        }
    }
}