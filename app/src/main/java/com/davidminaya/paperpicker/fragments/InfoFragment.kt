package com.davidminaya.paperpicker.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.adapters.InfoOptionAdapter
import com.davidminaya.paperpicker.dao.InfoOption
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.models.WallpaperModel
import com.davidminaya.paperpicker.utils.*
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_info.*
import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.util.*

class InfoFragment: BottomSheetDialogFragment() {

    private lateinit var model: WallpaperModel
    private lateinit var wallpaper: Wallpaper
    private lateinit var onWallpaperDeleteListener: () -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(activity!!)[WallpaperModel::class.java]
        wallpaper = arguments?.getParcelable(EXTRA_WALLPAPER)!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        context?.theme?.applyStyle(R.style.AppTheme, true)
        return LayoutInflater.from(activity!!).inflate(R.layout.fragment_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        titulo.text = wallpaper.name
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = InfoOptionAdapter(activity!!, createListOptions(), this::onItemClickListener)
    }

    private fun onItemClickListener(tag: String) {

        when(tag) {

            OPEN_LINK -> {

            }

            COPY_LINK -> {

            }

            EDIT_NAME -> {
                editNameDialog()
            }

            DELETE_WALLPAPER -> {
                deleteWallpaper()
            }
        }
    }

    @SuppressLint("InflateParams")
    private fun editNameDialog() {

        val inflater = LayoutInflater.from(activity).inflate(R.layout.dialog_edit_name, null)
        val editarNombre = inflater.findViewById<EditText>(R.id.editarNombre)
        editarNombre.setText(wallpaper.name)

        val dialog = AlertDialog.Builder(activity!!).apply {
            setTitle(getString(R.string.editar_nombre))
            setView(inflater)
        }

        dialog.setPositiveButton(getString(R.string.aceptar)) { _, _ ->

            val nombre = editarNombre.text.toString()

            if (nombre.isBlank() && nombre == wallpaper.name)
                return@setPositiveButton

            titulo.text = nombre
            wallpaper.name = nombre
            model.editWallpaper(wallpaper)
            model.setNombre(nombre)

            val intent = Intent(ACTION_EDIT_NAME_WALLPAPER).apply {
                putExtra(EXTRA_WALLPAPER_ID, wallpaper.wallpaperId)
                putExtra(EXTRA_WALLPAPER_NAME, wallpaper.name)
            }

            LocalBroadcastManager.getInstance(activity!!).sendBroadcast(intent)

            dismiss()
        }

        dialog.setNegativeButton(getString(R.string.cancelar)) { _, _ ->  }

        dialog.create().show()
    }

    private fun deleteWallpaper() {

        AlertDialog.Builder(activity!!).apply {
            setTitle("Eliminar fondo de pantalla")
            setPositiveButton("Eliminar") { _, _ ->

                model.deleteWallpaper(wallpaper)
                onWallpaperDeleteListener()

                val intent = Intent(ACTION_DELETE_WALLPAPER).apply {
                    putExtra(EXTRA_WALLPAPER_ID, wallpaper.wallpaperId)
                }

                LocalBroadcastManager.getInstance(activity!!).sendBroadcast(intent)

                dismiss()
            }
            setNegativeButton("Cancelar") { _, _ -> }
            create().show()
        }
    }

    fun setOnDeleteWallpaperListener(listener: () -> Unit) {
        onWallpaperDeleteListener = listener
    }

    private fun createListOptions(): List<InfoOption> {

        val black = Color.BLACK
        val blue = ContextCompat.getColor(activity!!, R.color.colorAccent)
        val red = Color.RED
        val infoOptions = mutableListOf<InfoOption>()

        if (wallpaper.date.isNotEmpty()) {

            val fecha = LocalDate.parse(wallpaper.date, DateTimeFormatter.ISO_LOCAL_DATE)
            val formatter = DateTimeFormatter.ofPattern("EEEE dd 'de' MMMM 'del' YYYY", Locale.forLanguageTag("es"))
            val fechaFormateada = fecha.format(formatter)

            infoOptions.add(InfoOption(R.drawable.ic_date, fechaFormateada, black, false))
        }

        if (wallpaper.url.isNotEmpty()) {
            infoOptions.run {
                add(InfoOption(R.drawable.ic_internet, wallpaper.url, black, false, OPEN_LINK))
                add(InfoOption(R.drawable.ic_copy_link, getString(R.string.copiar_enlace), blue, tag = COPY_LINK))
            }
        }

        infoOptions.run {
            add(InfoOption(R.drawable.ic_edit, getString(R.string.editar_nombre), blue, tag = EDIT_NAME))
            add(InfoOption(R.drawable.ic_round_delete, getString(R.string.eliminar), red, tag = DELETE_WALLPAPER))
        }

        return infoOptions
    }

    companion object {

        fun newInstance(wallpaper: Wallpaper): InfoFragment {

            val bundle = Bundle().apply {
                putParcelable(EXTRA_WALLPAPER, wallpaper)
            }

            return InfoFragment().apply {
                arguments = bundle
            }
        }
    }
}