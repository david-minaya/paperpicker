package com.davidminaya.paperpicker.fragments

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.GridLayoutManager
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.activities.WallpaperActivity
import com.davidminaya.paperpicker.adapters.WallpaperAdapter
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.models.MainModel
import com.davidminaya.paperpicker.utils.*
import kotlinx.android.synthetic.main.fragment_main.*
import java.util.ArrayList

class MainFragment: Fragment() {

    private lateinit var model: MainModel
    private lateinit var adapter: WallpaperAdapter
    private lateinit var wallpapers: MutableList<Wallpaper>
    private lateinit var broadcastManager: LocalBroadcastManager
    private lateinit var broadcastReceiver: LocalBroadcastReceiver
    private var canStopRefreshing = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model = ViewModelProviders.of(activity!!)[MainModel::class.java]

        broadcastReceiver = LocalBroadcastReceiver()
        val filters = IntentFilter().apply {
            addAction(ACTION_EDIT_NAME_WALLPAPER)
            addAction(ACTION_DELETE_WALLPAPER)
            addAction(ACTION_SYNC_WALLPAPER)
            addAction(ACTION_SYNC_WALLPAPER_FINISH)
        }

        broadcastManager = LocalBroadcastManager.getInstance(activity!!)
        broadcastManager.registerReceiver(broadcastReceiver, filters)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = WallpaperAdapter(activity!!)
        adapter.setOnItemClickListener(this::onItemClickListener)
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        recyclerView.adapter = adapter

        swipeRefresh.setColorSchemeResources(R.color.colorAccent)
        swipeRefresh.isRefreshing = true
        swipeRefresh.setOnRefreshListener(this::onRefreshListener)

        model.wallpapers().observe(activity!!, Observer {

            wallpapers = it

            if (wallpapers.isEmpty()) {

                title.visibility = View.VISIBLE
                description.visibility = View.VISIBLE
                swipeRefresh.isRefreshing = false

                return@Observer

            } else {

                title.visibility = View.GONE
                description.visibility = View.GONE
            }

            adapter.wallpapers = wallpapers

            if (canStopRefreshing) {
                swipeRefresh.isRefreshing = false
            }
        })
    }

    private fun onItemClickListener(position: Int) {

        val intent = Intent(activity, WallpaperActivity::class.java).apply {
            putExtra(EXTRA_WALLPAPER_POSITION, position)
            putParcelableArrayListExtra(EXTRA_WALLPAPERS, wallpapers as ArrayList<out Parcelable>)
        }

        startActivity(intent)
    }

    private fun onRefreshListener() {
        Common.runSyncWorkers()
    }

    override fun onDestroy() {
        super.onDestroy()
        broadcastManager.unregisterReceiver(broadcastReceiver)
    }

    private inner class LocalBroadcastReceiver: BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {

            when (intent?.action) {

                ACTION_EDIT_NAME_WALLPAPER -> {

                    val wallpaperId = intent.getIntExtra(EXTRA_WALLPAPER_ID, 0)
                    val wallpaperName = intent.getStringExtra(EXTRA_WALLPAPER_NAME)

                    adapter.updateItem(wallpaperId, wallpaperName)
                }

                ACTION_DELETE_WALLPAPER -> {

                    val wallpaperId = intent.getIntExtra(EXTRA_WALLPAPER_ID, 0)
                    adapter.deleteItem(wallpaperId)
                }

                ACTION_SYNC_WALLPAPER -> {

                    swipeRefresh.isRefreshing = true
                }

                ACTION_SYNC_WALLPAPER_FINISH -> {

                    Log.i("MainFragment", "update list")
                    canStopRefreshing = false

                    model.wallpapers().observe(activity!!, Observer {
                        wallpapers = it
                        adapter.updateAllItems(wallpapers)
                    })

                    if (intent.getBooleanExtra(EXTRA_IS_FINISH, false)) {
                        canStopRefreshing = true
                        swipeRefresh.isRefreshing = false
                    }
                }
            }
        }
    }
}