package com.davidminaya.paperpicker.fragments

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.davidminaya.paperpicker.R
import android.view.animation.DecelerateInterpolator
import android.view.ViewAnimationUtils
import android.widget.Toast
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.work.*
import com.davidminaya.paperpicker.activities.FolderActivity
import com.davidminaya.paperpicker.adapters.SettingOptionsAdapter
import com.davidminaya.paperpicker.dao.ItemType
import com.davidminaya.paperpicker.dao.SettingOption
import com.davidminaya.paperpicker.models.MainModel
import com.davidminaya.paperpicker.utils.*
import com.davidminaya.paperpicker.workers.ChangeWallpaperWorker
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.api.services.drive.DriveScopes
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.fragment_setting.*
import java.util.concurrent.TimeUnit
import kotlin.math.hypot

class SettingFragment: Fragment() {

    private lateinit var model: MainModel
    private val settingOptions = mutableListOf<SettingOption>()
    private lateinit var adapter: SettingOptionsAdapter
    private lateinit var preferences: SharedPreferences
    private lateinit var intervalNames: Array<String>
    private lateinit var intervalMillis: IntArray

    private lateinit var googleSignInClient: GoogleSignInClient
    private var googleAccount: GoogleSignInAccount? = null
    private var firebaseAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // https://developers.google.com/identity/sign-in/android/start-integrating
        googleAccount = GoogleSignIn.getLastSignedInAccount(activity!!)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.request_id_token))
            .requestEmail()
            .requestScopes(Scope(DriveScopes.DRIVE_FILE))
            .build()

        googleSignInClient = GoogleSignIn.getClient(activity!!, gso)
        firebaseAuth = FirebaseAuth.getInstance()

        model = ViewModelProviders.of(activity!!)[MainModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_setting, container, false)
        view?.addOnLayoutChangeListener(OnLayoutChangeListener())
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferences = PreferenceManager.getDefaultSharedPreferences(activity)

        intervalNames = resources.getStringArray(R.array.interval_names)
        intervalMillis = resources.getIntArray(R.array.interval_millis)

        fillSettingOptionsList()
        adapter = SettingOptionsAdapter(activity!!, settingOptions, this::onItemClickListener)

        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.adapter = adapter
    }

    inner class OnLayoutChangeListener: View.OnLayoutChangeListener {

        override fun onLayoutChange(v: View?, left: Int, top: Int, right: Int, bottom: Int,
                                    oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {

            val radio = hypot(right.toDouble(), bottom.toDouble()).toInt()

            val reveal = ViewAnimationUtils.createCircularReveal(v, right, top, 0f, radio.toFloat())
            reveal.interpolator = DecelerateInterpolator(2f)
            reveal.duration = 1000

            reveal.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    view?.removeOnLayoutChangeListener(this@OnLayoutChangeListener)
                }
            })

            reveal.start()
        }
    }

    override fun onResume() {
        super.onResume()

        fillSettingOptionsList()
        adapter.updateAllOptions(settingOptions)
    }

    private fun onItemClickListener(tag: String, adapterPosition: Int) {

        when (tag) {

            TAG_AUTO_CHANGE_WALLPAPER -> {

                val autoChangeWallpaper = !preferences.getBoolean(KEY_AUTO_CHANGE_WALLPAPER, false)

                preferences.edit().run {
                    putBoolean(KEY_AUTO_CHANGE_WALLPAPER, autoChangeWallpaper)
                    apply()
                }

                if (autoChangeWallpaper) {

                    val intervalMillis = preferences.getInt(KEY_CHANGE_WALLPAPER_EVERY, 900000).toLong()

                    val changeWallpaperWorker = PeriodicWorkRequestBuilder<ChangeWallpaperWorker>(
                        intervalMillis,
                        TimeUnit.MILLISECONDS)
                        .build()

                    WorkManager.getInstance().enqueueUniquePeriodicWork(
                        "ChangeWallpaper",
                        ExistingPeriodicWorkPolicy.REPLACE,
                        changeWallpaperWorker
                    )

                } else {

                    WorkManager.getInstance().cancelUniqueWork("ChangeWallpaper")
                }
            }

            TAG_CHANGE_WALLPAPER_EVERY -> {

                dialogChangeWallpaperEvery(adapterPosition)
            }

            TAG_LOGIN -> {

                startActivityForResult(googleSignInClient.signInIntent, 21)
            }

            TAG_LOGOUT -> {

                googleSignInClient.signOut().addOnSuccessListener {

                    Toast.makeText(activity!!, getString(R.string.mensaje_cerrar_sesion), Toast.LENGTH_SHORT).show()

                    googleAccount = null
                    fillSettingOptionsList()
                    adapter.updateAllOptions(settingOptions)
                }
            }

            TAG_DIRECTORY -> {
                startActivity(Intent(activity, FolderActivity::class.java))
            }
        }
    }

    private fun dialogChangeWallpaperEvery(adapterPosition: Int) {

        var intervalMilli = preferences.getInt(KEY_CHANGE_WALLPAPER_EVERY, 900000)
        var position = intervalMillis.indexOf(intervalMilli)

        val alertDialog = AlertDialog.Builder(activity)
            .setTitle("Cambiar fondos cada")
            .setNegativeButton("Cancelar") { _, _ -> }

        alertDialog.setSingleChoiceItems(intervalNames, position) { _, which ->
            intervalMilli = intervalMillis[which]
            position = which
        }

        alertDialog.setPositiveButton("Aceptar") { _, _ ->

            preferences.edit().run {
                putInt(KEY_CHANGE_WALLPAPER_EVERY, intervalMilli)
                apply()
            }

            adapter.updateOptionWithSummary(adapterPosition, intervalNames[position])

            // Configura el trabajador que cambiara los fondos de pantalla

            val intervalMillis = preferences.getInt(KEY_CHANGE_WALLPAPER_EVERY, 900000).toLong()

            val changeWallpaperWorker = PeriodicWorkRequestBuilder<ChangeWallpaperWorker>(
                intervalMillis,
                TimeUnit.MILLISECONDS)
                .build()

            WorkManager.getInstance().enqueueUniquePeriodicWork(
                "ChangeWallpaper",
                ExistingPeriodicWorkPolicy.REPLACE,
                changeWallpaperWorker
            )
        }

        alertDialog.create().show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 21) {

            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {

                googleAccount = task.getResult(ApiException::class.java)
                val credential = GoogleAuthProvider.getCredential(googleAccount?.idToken, null)
                firebaseAuth?.signInWithCredential(credential)

                fillSettingOptionsList()
                adapter.updateAllOptions(settingOptions)

                Toast.makeText(activity, getString(R.string.bienvenido) +" "+ googleAccount?.email, Toast.LENGTH_SHORT).show()

                Common.runSyncWorkers()
                LocalBroadcastManager.getInstance(activity!!).sendBroadcast(Intent(ACTION_SYNC_WALLPAPER))

            } catch (e: ApiException) {

                Toast.makeText(activity, getString(R.string.mensaje_error_inicio_de_sesion), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun fillSettingOptionsList() {

        val intervalMillis = preferences.getInt(KEY_CHANGE_WALLPAPER_EVERY, 900000)
        val position = this.intervalMillis.indexOf(intervalMillis)

        val isAutoChangeWallpaper = preferences.getBoolean(KEY_AUTO_CHANGE_WALLPAPER, false)
        val summaryChangeWallpaper = intervalNames[position]

        val categoriaConfiguracionDeFondos = SettingOption().apply {
            title = getString(R.string.configuracion_de_fondos)
            itemType = ItemType.OPTION_CATEGORY
            isClickable = false
        }

        val cambiarFondosAutomaticamente = SettingOption().apply {
            title = getString(R.string.cambiar_fondos_automaticamente)
            itemType = ItemType.SWITCH_OPTION
            switchIsChecked = isAutoChangeWallpaper
            tag = TAG_AUTO_CHANGE_WALLPAPER
        }

        val cambiarFondoCada = SettingOption().apply {
            title = getString(R.string.cambiar_fondos_cada)
            summary = summaryChangeWallpaper
            itemType = ItemType.OPTION_WITH_SUMMARY
            isEnable = isAutoChangeWallpaper
            tag = TAG_CHANGE_WALLPAPER_EVERY
        }

        val categoriaCuenta = SettingOption().apply {
            title = getString(R.string.cuenta)
            itemType = ItemType.OPTION_CATEGORY
            isClickable = false
        }

        val iniciarSesion = SettingOption().apply {
            title = getString(R.string.iniciar_sesion)
            itemType = ItemType.SINGLE_OPTION
            tag = TAG_LOGIN
        }

        val cuenta = SettingOption().apply {
            title = googleAccount?.email ?: ""
            itemType = ItemType.OPTION_WITH_SUMMARY
            summary = "Google Drive"
            isClickable = false
        }

        val cerrarSesion = SettingOption().apply {
            title = getString(R.string.cerrar_sesion)
            itemType = ItemType.SINGLE_OPTION
            tag = TAG_LOGOUT
        }

        val categoriaCarpetas = SettingOption().apply {
            title = getString(R.string.carpeta)
            itemType = ItemType.OPTION_CATEGORY
            isClickable = false
        }

        val carpetas = SettingOption().apply {
            title = getString(R.string.carpetas_agregadas)
            itemType = ItemType.OPTION_WITH_SUMMARY
            summary = getSummaryCarpetas()
            tag = TAG_DIRECTORY
        }

        settingOptions.clear()

        settingOptions.run {

            add(categoriaConfiguracionDeFondos)
            add(cambiarFondosAutomaticamente)
            add(cambiarFondoCada)
            add(categoriaCuenta)

            if (googleAccount == null) {
                add(iniciarSesion)
            } else {
                add(cuenta)
                add(cerrarSesion)
            }
            add(categoriaCarpetas)
            add(carpetas)
        }
    }

    private fun getSummaryCarpetas(): String {

        val uriPermissions = activity?.contentResolver?.persistedUriPermissions!!

        var summary = ""

        when {

            uriPermissions.isEmpty() -> {
                summary = "Aun no ha agregado carpetas"
            }

            uriPermissions.size == 1 -> {
                summary = DocumentFile.fromTreeUri(activity!!, uriPermissions[0].uri)?.name!!
            }

            uriPermissions.size == 2 -> {
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[0].uri)?.name!! + ", "
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[1].uri)?.name!!
            }

            uriPermissions.size == 3 -> {
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[0].uri)?.name!! + ", "
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[1].uri)?.name!! + ", "
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[2].uri)?.name!!
            }

            uriPermissions.size == 4 -> {
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[0].uri)?.name!! + ", "
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[1].uri)?.name!! + ", "
                summary += DocumentFile.fromTreeUri(activity!!, uriPermissions[2].uri)?.name!! + " ..."
            }
        }

        return summary
    }
}