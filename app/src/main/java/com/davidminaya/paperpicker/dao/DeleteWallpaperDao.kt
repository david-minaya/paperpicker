package com.davidminaya.paperpicker.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.davidminaya.paperpicker.entitys.DeleteWallpaper

@Dao
interface DeleteWallpaperDao {

    @Query("SELECT * FROM DeleteWallpaper")
    fun deletesWallpapers(): List<DeleteWallpaper>

    @Insert
    fun insert(vararg deleteWallpaper: DeleteWallpaper)

    @Delete
    fun delete(vararg deleteWallpaper: DeleteWallpaper)
}