package com.davidminaya.paperpicker.dao

class InfoOption(val icono: Int, val texto: String, val color: Int,
                 val isClickable: Boolean = true, val tag: String = "")