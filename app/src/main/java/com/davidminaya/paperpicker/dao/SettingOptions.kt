package com.davidminaya.paperpicker.dao

class SettingOption {

    lateinit var title: String
    lateinit var itemType: ItemType
    var isClickable: Boolean = true
    var tag: String? = null
    var summary: String? = null
    var switchIsChecked = false
    var isEnable = true
}

enum class ItemType {
    OPTION_CATEGORY,
    SWITCH_OPTION,
    SINGLE_OPTION,
    OPTION_WITH_SUMMARY
}