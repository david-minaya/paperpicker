package com.davidminaya.paperpicker.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.davidminaya.paperpicker.entitys.Wallpaper

@Dao
interface WallpaperDao {

    @Query("SELECT * FROM wallpaper")
    fun wallpapersAsync(): LiveData<List<Wallpaper>>

    @Query("SELECT * FROM wallpaper")
    fun wallpapers(): MutableList<Wallpaper>

    @Query("SELECT * FROM wallpaper WHERE wallpaperId = :wallpaperId")
    fun wallpaper(wallpaperId: Int): Wallpaper

    @Query("SELECT wallpaperId FROM wallpaper")
    fun wallpapersId(): LiveData<List<Int>>

    @Query("SELECT path FROM wallpaper")
    fun wallpapersPaths(): MutableList<String>

    @Query("SELECT md5 FROM wallpaper")
    fun md5Hashes(): List<String>

    @Query("SELECT * FROM wallpaper WHERE md5 = :md5")
    fun wallpaper(md5: String): Wallpaper

    @Insert
    fun insert(vararg wallpapers: Wallpaper)

    @Delete
    fun deleteWallpapers(vararg wallpaper: Wallpaper)

    @Update
    fun updateWallpapers(vararg wallpapers: Wallpaper)
}