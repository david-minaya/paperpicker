package com.davidminaya.paperpicker.entitys

import android.graphics.Bitmap
import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import java.io.File

@Entity
class Wallpaper() : Parcelable {

    @PrimaryKey
    var wallpaperId: Int? = null
    var name = ""
    var path = ""
    var date = ""
    var url = ""
    var driveId: String? = null
    var md5: String? = null

    @Ignore
    var file: File? = null

    @Ignore
    var bitmap: Bitmap? = null

    constructor(parcel: Parcel) : this() {
        wallpaperId = parcel.readValue(Int::class.java.classLoader) as? Int
        name = parcel.readString()!!
        path = parcel.readString()!!
        date = parcel.readString()!!
        url = parcel.readString()!!
        md5 = parcel.readString()
        driveId = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(wallpaperId)
        parcel.writeString(name)
        parcel.writeString(path)
        parcel.writeString(date)
        parcel.writeString(url)
        parcel.writeString(md5)
        parcel.writeString(driveId)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Wallpaper

        if (wallpaperId != other.wallpaperId) return false
        if (name != other.name) return false
        if (path != other.path) return false
        if (date != other.date) return false
        if (url != other.url) return false
        if (md5 != other.md5) return false
        if (driveId != other.driveId) return false

        return true
    }

    override fun hashCode(): Int {

        var result = wallpaperId ?: 0
        result = 31 * result + name.hashCode()
        result = 31 * result + path.hashCode()
        result = 31 * result + date.hashCode()
        result = 31 * result + url.hashCode()
        result = 31 * result + md5.hashCode()
        result = 31 * result + driveId.hashCode()

        return result
    }

    override fun toString(): String {
        return "Wallpaper(wallpaperId=$wallpaperId, " +
                "         name='$name', " +
                "         path='$path', " +
                "         date='$date', " +
                "         url='$url', " +
                "         file=$file, " +
                "         md5=$md5" +
                "         driveId=$driveId)"
    }


    companion object CREATOR : Parcelable.Creator<Wallpaper> {
        override fun createFromParcel(parcel: Parcel): Wallpaper {
            return Wallpaper(parcel)
        }

        override fun newArray(size: Int): Array<Wallpaper?> {
            return arrayOfNulls(size)
        }
    }
}