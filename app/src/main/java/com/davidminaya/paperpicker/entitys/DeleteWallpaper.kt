package com.davidminaya.paperpicker.entitys

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class DeleteWallpaper() {

    @PrimaryKey
    var deleteWallpaperId: Int? = null
    var md5: String? = null
    var deleteDriveId: String? = null

    constructor(md5: String, deleteDriveId: String?): this() {
        this.md5 = md5
        this.deleteDriveId = deleteDriveId
    }
}