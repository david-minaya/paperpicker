package com.davidminaya.paperpicker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.entitys.Wallpaper
import com.davidminaya.paperpicker.adapters.WallpaperAdapter.ViewHolder

class WallpaperAdapter(private val context: Context): RecyclerView.Adapter<ViewHolder>() {

    private lateinit var onItemClickListener: (Int) -> Unit

    var wallpapers = mutableListOf<Wallpaper>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, itemType: Int): ViewHolder {
        val imageView = LayoutInflater.from(context).inflate(R.layout.item_wallpaper_phone, parent, false) as ImageView
        return ViewHolder(imageView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.imageView.setImageBitmap(wallpapers[position].bitmap)
    }

    override fun getItemCount(): Int = wallpapers.size

    fun setOnItemClickListener(listener: (Int) -> Unit) {
        onItemClickListener = listener
    }

    fun updateItem(wallpaperId: Int, wallpaperName: String) {

        val wallpaper = wallpapers.find { it.wallpaperId == wallpaperId } ?: return
        val index = wallpapers.indexOf(wallpaper)

        wallpaper.name = wallpaperName
        notifyItemChanged(index)
    }

    fun updateAllItems(wallpapers: MutableList<Wallpaper>) {

        this.wallpapers = wallpapers
        notifyDataSetChanged()
    }

    fun deleteItem(wallpaperId: Int) {

        val wallpaper = wallpapers.find { it.wallpaperId == wallpaperId } ?: return
        val index = wallpapers.indexOf(wallpaper)

        wallpapers.remove(wallpaper)
        notifyItemRemoved(index)
    }

    inner class ViewHolder(val imageView: ImageView): RecyclerView.ViewHolder(imageView) {

        init {
            imageView.setOnClickListener {
                onItemClickListener(adapterPosition)
            }
        }
    }
}