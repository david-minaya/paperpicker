package com.davidminaya.paperpicker.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.adapters.InfoOptionAdapter.ViewHolder
import com.davidminaya.paperpicker.dao.InfoOption

class InfoOptionAdapter(private val context: Context,
                        private val infoOptions: List<InfoOption>,
                        private val onItemClickListener: (String) -> Unit): RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = LayoutInflater.from(context).inflate(R.layout.item_info_option, parent, false)
        return ViewHolder(layout)
    }

    override fun getItemCount(): Int = infoOptions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val infoOption = infoOptions[position]

        holder.run {
            icono = infoOption.icono
            texto = infoOption.texto
            color = infoOption.color
            isClickable = infoOption.isClickable
            tag = infoOption.tag
        }
    }

    inner class ViewHolder(private val layout: View): RecyclerView.ViewHolder(layout) {

        private val imageView = layout.findViewById<ImageView>(R.id.icono)
        private val textView = layout.findViewById<TextView>(R.id.texto)

        init {
            layout.setOnClickListener {
                onItemClickListener(layout.tag as String)
            }
        }

        var icono = R.drawable.ic_info
            set(value) {
                field = value
                imageView.setImageResource(icono)
            }

        var texto = ""
            set(value) {
                field = value
                textView.text = field
            }

        var color = Color.BLACK
            set(value) {
                field = value
                textView.setTextColor(color)
            }

        var isClickable = true
            set(value) {
                field = value
                layout.isClickable = field
                layout.isFocusable = field
            }

        var tag = ""
            set(value) {
                field = value
                layout.tag = field
            }
    }
}