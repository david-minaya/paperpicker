package com.davidminaya.paperpicker.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Switch
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.adapters.SettingOptionsAdapter.ViewHolder
import com.davidminaya.paperpicker.dao.ItemType
import com.davidminaya.paperpicker.dao.SettingOption

class SettingOptionsAdapter(private val context: Context,
                            private var settingOptions: List<SettingOption>,
                            private val onItemClickListener: (String, Int) -> Unit): RecyclerView.Adapter<ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view: View?

        when (viewType) {

            ItemType.OPTION_CATEGORY.ordinal -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_setting_category, parent, false)
            }

            ItemType.SWITCH_OPTION.ordinal -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_setting_switch_option, parent, false)
            }

            ItemType.OPTION_WITH_SUMMARY.ordinal -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_setting_option_with_summary, parent, false)
            }

            ItemType.SINGLE_OPTION.ordinal -> {
                view = LayoutInflater.from(context).inflate(R.layout.item_setting_single_option, parent, false)
            }

            else -> {
                view = null
            }
        }

        return ViewHolder(view!!)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val settingOption = settingOptions[position]

        holder.title?.text = settingOption.title
        holder.itemView.tag = settingOption.tag
        holder.summary?.text = settingOption.summary
        holder.itemView.isClickable = settingOption.isClickable
        holder.itemView.isFocusable = settingOption.isClickable

        holder.setSwitchChecked(settingOption.switchIsChecked)
        holder.setEnableItem(settingOption.isEnable, settingOption.isClickable)
    }

    override fun getItemViewType(position: Int): Int {
        return settingOptions[position].itemType.ordinal
    }

    override fun getItemCount(): Int = settingOptions.size

    fun updateOptionWithSummary(position: Int, summary: String) {
        settingOptions[position].summary = summary
        notifyItemChanged(position)
    }

    fun updateAllOptions(settingOptions: List<SettingOption>) {
        this.settingOptions = settingOptions
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val item: View): RecyclerView.ViewHolder(item) {

        val title = item.findViewById<TextView?>(R.id.title)
        val switch = item.findViewById<Switch?>(R.id.autoChangeWallpaper)
        val summary = item.findViewById<TextView?>(R.id.summary)

        init {

            item.setOnClickListener {

                onItemClickListener(item.tag.toString(), adapterPosition)

                if (switch != null) {

                    val isChecked = !switch.isChecked

                    setSwitchChecked(isChecked)
                    toggleEnableItem(isChecked)
                }
            }

            switch?.setOnCheckedChangeListener(this::onCheckedChangeListener)
        }

        private fun onCheckedChangeListener(buttonView: View, isChecked: Boolean) {
            onItemClickListener(item.tag.toString(), adapterPosition)
            toggleEnableItem(isChecked)
        }

        fun setSwitchChecked(isChecked: Boolean) {

            if (switch != null) {
                switch.setOnCheckedChangeListener(null)
                switch.isChecked = isChecked
                switch.setOnCheckedChangeListener(this::onCheckedChangeListener)
            }
        }

        fun setEnableItem(isEnable: Boolean, isClickable: Boolean) {

            title?.isEnabled = isEnable
            switch?.isEnabled = isEnable
            summary?.isEnabled = isEnable

            if (isClickable) {
                itemView.isClickable = isEnable
                itemView.isFocusable = isEnable
            }
        }

        private fun toggleEnableItem(isEnable: Boolean) {
            settingOptions[2].isEnable = isEnable
            notifyItemChanged(2)
        }
    }
}