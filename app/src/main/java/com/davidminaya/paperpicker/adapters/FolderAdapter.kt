package com.davidminaya.paperpicker.adapters

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.UriPermission
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.documentfile.provider.DocumentFile
import androidx.recyclerview.widget.RecyclerView
import com.davidminaya.paperpicker.R
import com.davidminaya.paperpicker.adapters.FolderAdapter.ViewHolder

class FolderAdapter(private val context: Context): RecyclerView.Adapter<ViewHolder>() {

    var uriPermissions =  mutableListOf<UriPermission>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    lateinit var onRemovedFolderListener: () -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context).inflate(R.layout.item_folder, parent, false)
        return ViewHolder(inflater)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val folder = DocumentFile.fromTreeUri(context, uriPermissions[position].uri)
        holder.name.text = folder?.name
    }

    override fun getItemCount() = uriPermissions.size

    inner class ViewHolder(view: View): RecyclerView.ViewHolder(view) {

        val name = view.findViewById<TextView>(R.id.name)!!
        private val delete = view.findViewById<ImageView>(R.id.delete)!!

        init {
            delete.setOnClickListener(this::onClickDeleteListener)
        }

        private fun onClickDeleteListener(view: View) {

            AlertDialog.Builder(context)
                .setTitle("Remover carpeta")
                .setMessage("Esta seguro de remover la carpeta ${name.text}")
                .setPositiveButton("Remover", this::onClickPositiveButtonListener)
                .setNegativeButton("Cancelar", null)
                .create()
                .show()
        }

        private fun onClickPositiveButtonListener(dialog: DialogInterface, which: Int) {

            val uriPermission = uriPermissions[adapterPosition]
            val flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION

            context.contentResolver.releasePersistableUriPermission(uriPermission.uri, flags)

            uriPermissions.remove(uriPermission)
            notifyDataSetChanged()

            Toast.makeText(context, "Carpeta ${name.text} removida", Toast.LENGTH_SHORT).show()
            onRemovedFolderListener()
        }
    }
}